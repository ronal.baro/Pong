﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMana : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerControl _player1;
    public PlayerControl _player2;
    public BallScript _ball;
    private Rigidbody2D _rbPlayer1;
    private Rigidbody2D _rbPlayer2;
    private Rigidbody2D _rbBall;
    private CircleCollider2D _ballCollider;
    private bool _isDebugWindowShown = false;

    public Trajectory trajectory;


    public int _maxScore;
    void Start()
    {
        _rbPlayer1 = _player1.GetComponent<Rigidbody2D>();
        _rbPlayer2 = _player2.GetComponent<Rigidbody2D>();
        _rbBall = _ball.GetComponent<Rigidbody2D>();
        _ballCollider = _ball.GetComponent<CircleCollider2D>();

    }



    private void OnGUI()
    {
        //BASE UI
        GUI.Label(new Rect((Screen.width / 2) - 150 - 12, 20, 100, 100), "" + _player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + _player2.Score);

        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "Restart"))
        {
            _player1.ResetScore();
            _player2.ResetScore();

            _ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }

        if (_player1.Score >= _maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER ONE WINS");
            _ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }
        else if (_player2.Score >= _maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER TWO WINS");
            _ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        //DEBUG WINDOW

        if (_isDebugWindowShown){
            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor =  Color.red;

            float ballMass=_rbBall.mass;
            float ballSpeed =_rbBall.velocity.magnitude;
            float ballFriction = _ballCollider.friction;
            Vector2 ballVelocity  = _rbBall.velocity;
            Vector2 ballMomentum = ballMass * ballVelocity;
            
            float impulsePlayer1X = _player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = _player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = _player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = _player2.LastContactPoint.tangentImpulse;

            string debugText = "Ball Mass = " + ballMass + "\n" +
                               "Ball Velocity = " + ballVelocity + "\n" +
                               "Ball Speed = " + ballSpeed + "\n" +
                               "Ball Momentum = " + ballMomentum + "\n" +
                               "Ball Friction = " + ballFriction + "\n" +
                               "Last Impulse from Player 1 = (" + impulsePlayer1X + ", " +impulsePlayer1Y + ")\n" +
                               "Last Impulse from Player 2 = (" + impulsePlayer2X + ", " +impulsePlayer2Y + ")\n" ;
                               
            GUIStyle gUIStyle = new GUIStyle(GUI.skin.textArea);
            gUIStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea( new Rect(Screen.width/2 - 200, Screen.height -200, 400, 110),debugText,gUIStyle);

            GUI.backgroundColor = oldColor;
        }
        // DEBUG BUTTON

        if (GUI.Button(new Rect(Screen.width/2 - 60,  Screen.height - 73, 120,53),"Toggle\nDebug Info"))
        {
            _isDebugWindowShown = !_isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;

        }

    }



}

