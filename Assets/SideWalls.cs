﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWalls : MonoBehaviour
{
    // Start is called before the first frame update

    public PlayerControl _player;
    [SerializeField] private GameMana gameManager;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D anotherCollider) {
        if (anotherCollider.name == "Ball"){
            _player.IncrementScore();
            if (_player.Score<gameManager._maxScore){
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
