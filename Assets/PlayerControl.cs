using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    private Rigidbody2D _rigidBody2D;
    private int _score;

    private ContactPoint2D _lastContactPoint;
    public float _speed = 10.0f;
    public float _yBoundary = 9.0f;
    public KeyCode _upButton = KeyCode.W;
    public KeyCode _downButton = KeyCode.S;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velocity = _rigidBody2D.velocity;

        if (Input.GetKey(_upButton))
        {
            velocity.y = _speed;
        }
        else if (Input.GetKey(_downButton))
        {
            velocity.y = -_speed;
        }
        else velocity.y = 0.0f;

        _rigidBody2D.velocity = velocity;

        Vector2 position = transform.position;

        if (position.y > _yBoundary)
        {
            position.y = _yBoundary;
            transform.position = position;
        }
        else if (position.y < -_yBoundary)
        {
            position.y = -_yBoundary;
            transform.position = position;
        }
    }

    public void IncrementScore()
    {
        _score++;
    }

    public void ResetScore()
    {
        _score = 0;
    }

    public int Score
    {
        get { return _score; }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Equals("Ball"))
        {
            _lastContactPoint = other.GetContact(0);
            
        }
    }
    


    public ContactPoint2D LastContactPoint
    {
        get { return _lastContactPoint; }
    }

}
