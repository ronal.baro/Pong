﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    // Start is called before the first frame update
    public BallScript _ball;
    CircleCollider2D _ballCollider;
    Rigidbody2D _rbBall;

    public GameObject ballAtCollison;



    void Start()
    {
        _rbBall = _ball.GetComponent<Rigidbody2D>();
        _ballCollider = _ball.GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bool drawBallAtCollision = false;
        Vector2 offsetHitPoint = new Vector2();

        RaycastHit2D[] circleCastHit2DArray = Physics2D.CircleCastAll(_rbBall.position, _ballCollider.radius, _rbBall.velocity.normalized);
        foreach (RaycastHit2D circleCastHit2D in circleCastHit2DArray)
        {
            if (circleCastHit2D.collider != null && circleCastHit2D.collider.GetComponent<BallScript>() == null)
            {
                Vector2 hitPoint = circleCastHit2D.point;

                Vector2 hitNormal = circleCastHit2D.normal;

                offsetHitPoint = hitPoint + hitNormal * _ballCollider.radius;

                DottedLine.DottedLine.Instance.DrawDottedLine(_ball.transform.position, offsetHitPoint);
                if (circleCastHit2D.collider.GetComponent<SideWalls>() == null)
                {
                    Vector2 inVector = (offsetHitPoint - _ball.TrajectoryOrigin).normalized;

                    Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                    float outDot = Vector2.Dot(outVector, hitNormal);
                    if (outDot > -1.0f && outDot < 1.0)
                    {
                        DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint, offsetHitPoint + outVector * 10.0f);

                        drawBallAtCollision = true;
                    }
                }

                break;

            }
            if (drawBallAtCollision)
            {
                ballAtCollison.transform.position = offsetHitPoint;
                ballAtCollison.SetActive(true);
            }
            else
            {
                ballAtCollison.SetActive(false);
            }

        }
    }
}
