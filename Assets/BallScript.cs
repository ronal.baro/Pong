﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D _rbBall;
    //public float _xInitialForce;
    //public float _yInitialForce;
    public float _power;
    private Vector2 _trajectoryOrigin;
    void Start()
    {
        _rbBall = GetComponent<Rigidbody2D>();
        _trajectoryOrigin = transform.position;
        RestartGame();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ResetBall()
    {

        transform.position = Vector2.zero;
        _rbBall.velocity = Vector2.zero;

    }

    void PushBall()
    {
        //RANDOM DIRECTION BUT RANDOM SPEED TOO.

        // float yRandomInitialForce = Random.Range(-_yInitialForce, _yInitialForce);
        // float randomDirection = Random.Range(0, 2);

        // if (randomDirection < 1.0f)
        // {
        //     _rbBall.AddForce(new Vector2(-_xInitialForce, yRandomInitialForce));
        // }
        // else
        // {
        //     _rbBall.AddForce(new Vector2(_xInitialForce, yRandomInitialForce));
        // }


        //RANDOM DIRECTION + CONSTANT SPEED
        float dirX;
        do { dirX = Random.Range(-1.0f, 1.0f); } while (dirX > -0.2f && dirX < 0.2f);

        float dirY = Random.Range(-0.7f, .7f);

        //do while sama 0.7 agar arahnya tidak terlalu tegak lurus.


        //Debug.Log(dirX + " " + dirY);

        //jika formatnya tidak 1.0f,  hanya akan mengacak bilangan INT antara -1, 0 , 1.


        _rbBall.AddForce(new Vector2(dirX, dirY).normalized * _power);
        //normalized agar tidak ada magnitude.. 


    }

    void RestartGame()
    {
        ResetBall();

        Invoke("PushBall", 2);
    }

    void OnCollisionExit2D(Collision2D other)
    {
        _trajectoryOrigin = transform.position;
    }

    public Vector2 TrajectoryOrigin
    {
        get { return _trajectoryOrigin; }
    }

}
